"""
import komutu başka bir Python dosyasında tanımlanmış herhangi bir şeyi o esnada çalıştığımız dosyaya çeker.
Bunlar değişken, fonksiyon, class vb olabilir.
"""
import random

"""
random, python içerisinde rastgele bir şeyler elde etmemizi sağlayan bir kütüphanedir.
randint ise random kütüphanesi içerisinde belirttiğimiz aralıklar içinde rastgele bir sayı oluşturan bir fonsksiyondur.
"""
print(random.randint(0, 100))

"""
from `x` import `y` kalıbını kullanarak kütüphanenin tamamını değil de sadece ihtiyaç duyduğumuz
parçasını çekebiliyoruz. Bu senaryoda `random` kütüphanesi içerisinden `randint` fonskiyonu çekilmiş oldu.
Bu senaryoda `random` kutuphanesinin içerisindeki diğer hiçbir veri kodumuzda açık değildir.
Bu sayede de kodumuz daha az bellek tüketecektir.
"""

from random import randint

print(randint(100, 1000))

"""
Bir kütüphaneden birden fazla obje çekmek istiyorsak eğer bunun için öbjeleri virgülle ayırıyoruz. Ör:
from X import Y, Z
"""

from math import floor, sin

print(floor(20.6))
print(sin(270.0))

"""
çekmek istediğimiz içerik projemizdeki baska bir dosyanın da parçası olabilir. Bu durumda dosyanın adını kullanarak cağırıyoruz
"""

from d11_fonksiyonlar import coklu_returnlu_fonksiyon

print(coklu_returnlu_fonksiyon())

import d13_1_main

d13_1_main.get_name()
print("")
d13_1_main.main()


from ornekler.o03_ogrenci_listesi import get_input

a = get_input("Bana bir input ver abi")
print(a)
