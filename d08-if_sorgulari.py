if 12 > 10:  # True
    print('12, 10dan buyuktur')


if 10 > 12:  # False
    print('10, 12den buyuktur')


if 22 == 10:
    print('22, 10a esit')
elif 22 > 10:
    print('22, 10dan buyuktur')
elif 22 < 10:
    print('22, 10dan kucuktur')
else:
    print('aga bu ne')

if 42 == 10:
    print('42, 10a esit')
if 42 > 10:
    print('42, 10dan buyuktur')
if 42 < 10:
    print('42, 10dan kucuktur')

a = 'Ali'
b = None

if a:
    print('a var abi')
else:
    print('a yok abi')

if b:
    print('b var abi')
else:
    print('b yok abi')


if 'ahmet' in ['ali', 'veli', 'ahmet'] and 12 == 12:
    print('ahmet listede var ve 12 ile 12 esit')

if 'ahmet' in ['ali', 'veli', 'ahmet']:
    if 12 == 12:
        print('ahmet listede var ve 12 ile 12 esit')
