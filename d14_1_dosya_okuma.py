"""
Dosya okuma için open komutunu kullanıyoruz.
Argüman olarak dosyanın komunu ve okuma modunu veriyoruz.
Eğer herhangi bir okuma modu vermezsek, default hali "r" yani "read".

Dosya açma modları:
- r: okuma
- w: yazma
- a: sondan ekleme
- x: oluşturma
- +: okuma ve yazma
"""
dosya = open('d14_okunacak_dosya.txt', 'r')

# read() komutu dosyayi komple okur
# icerik = dosya.read()
# print(icerik)

# readlines komutu bize satırları listeye çevirerek verir.
icerik_listesi = dosya.readlines()

# close komutu ile programımıza dosyayı okumasını bırakmayı söylüyoruz
dosya.close()

print(icerik_listesi)

for index, value in enumerate(icerik_listesi):
    print(f"{index} indexindeki satır:")
    print(value)
