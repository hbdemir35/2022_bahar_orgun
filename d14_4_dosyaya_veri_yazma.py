ogrenci_listesi = [
    {
        'name': 'Mahmut',
        'surname': 'Tuncer',
        'age': '99',
        'classroom': 'Flash TV',
    },
    {
        'name': 'İbrahim',
        'surname': 'Tatlıses',
        'age': '82',
        'classroom': 'IboShow',
    },
    {
        'name': 'Müslüm',
        'surname': 'Gürses',
        'age': 'infinity',
        'classroom': 'our_hearts',
    },
]

with open('student_list.txt', 'w') as file:
    output = ''
    for i in ogrenci_listesi:
        output += '%'.join(i.values()) + '\n'
    file.write(output)
