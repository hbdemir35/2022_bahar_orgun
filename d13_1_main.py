"""
Bir şeyi importlarken eğer importladığı kaynakta çalışan bir şey varsa eğer
importladığımız yer de bunu çalıştırır. Buna engel olmanın python içerisinde çok kolay bir yolu vardır
Bu da __main__ sorgusunu kullanmaktır
"""


def mahmut():
    print("Mahmut Tuncer")


# eğer burayı bu şekilde bırakırsak `mahmut` fonksiyonunu importladığımız her yerde otomatik çalışacak
# mahmut()


def get_name():
    print(f"{__name__=}")


def main():
    mahmut()
    get_name()


if __name__ == "__main__":
    main()
