class Hayvan:
    def __init__(self, name: str, age: int, gender: str) -> None:
        self.name = name
        self.age = age
        self.gender = gender
        self.alive = True

    def __str__(self) -> str:
        return self.name

    def get_old(self) -> int:
        if self.alive:
            self.age += 1
        return self.age

    def get_description(self) -> str:
        prefix = 'She' if self.gender == 'Female' else 'He'
        year = 'year' if self.age == 1 else 'years'
        _is = 'is' if self.alive else 'was'
        return f'{self.name}: {prefix} {_is} a {self.age} {year} old animal.'


# Bu Kedi classı, önceden oluşturduğumuz Hayvan classının bütün özelliklerine sahiptir.
class Kedi(Hayvan):
    def __init__(self, name: str, age: int, gender: str, colors: list[str]) -> None:
        self.name = name
        self.age = age
        self.gender = gender
        self.alive = True
        self.type = 'Cat'
        self.colors = colors

    def get_colors(self) -> str:
        colors = ', '.join(self.colors)
        if len(self.colors) == 3 and self.gender == 'Female':
            return f'{self.name} is a Tri-color cat with {colors} colors.'
        elif len(self.colors) == 3:
            return f'{self.name} is a extremely rare male Tri-color cat with {colors} colors.'
        elif len(self.colors) == 2:
            return f'{self.name} is a cat with {colors} colors.'
        elif len(self.colors) == 1:
            return f'{self.name} is a {colors} cat.'
        elif len(self.colors) == 0:
            return f'{self.name} is an invisible cat...'
        else:
            return f'{self.name} is not a cat, it\'s a rainbow...'

    def get_description(self) -> str:
        base = super().get_description()
        return base.replace('animal.', f'{self.type.lower()}.')


class Kopek(Hayvan):
    def __init__(self, name: str, age: int, gender: str, breed: str) -> None:
        # Super, class'ın bir üst ebeveyn classının özelliklerini/methodlarını direkt çağırmamızı sağlar
        # Bu sayede önceki yetenekleri yeniden yazmamıza gerek kalmadan üzerlerine ekleme yapabiliriz.
        super().__init__(name, age, gender)
        self.type = 'Dog'
        self.breed = breed

    def get_description(self) -> str:
        base = super().get_description()
        _type = f'{self.breed.lower()} {self.type.lower()}'
        return base.replace('animal.', f'{_type}.')


def main():
    pacchi = Kedi('Pacchi', 2, 'Female', ['black', 'brown', 'white'])

    print(pacchi)
    print(type(pacchi))
    print(pacchi.get_colors())
    print(pacchi.age)
    print(pacchi.get_old())

    findik = Kedi('Fındık', 3, 'Female', ['gray'])

    print(findik.get_colors())

    glock = Kopek('Glock', 1, 'Male', 'Belçika Kurdu')

    candy = Kedi('Candy', 19, 'Female', ['black', 'brown', 'white'])
    candy.alive = False

    print(glock.name, glock.age, glock.gender, glock.breed)
    print(pacchi.get_description())
    print(candy.get_description())
    print(glock.get_description())


if __name__ == "__main__":
    main()
