def get_input(
        text, is_num=False, is_opt=False,
        opt_list=['+', '-', '*', '/', '%']
):
    """
    Bu fonksiyon programımız için almamız gereken bütün inputları işleyen fonksiyondur.

    text: input esnasında ekrana yazdıracağımız yazı.
    is_num: bu argüman True ise fonksiyon inputun sayı olup olmadığını deneyleyecektir.
    is_opt: bu argüman True ise fonsiyon inputun bir operatör olup olmadığını denetleyecektir.
    opt_list: bu da operatör seçeneklerimizi belirten bir listedir
    """
    inp = input(f'{text}: ')
    # Burada aldığımız inputu önce küçük harfli hale getirip ardından "q" ile kıyaslıyoruz.

    if inp.lower() == 'q':
        print('Programdan başarılı bir biçimde çıktınız.')
        exit()

    # Burada eğer fonksiyonu çağırırken "is_num=True" olarak tanımlanmışsayi2
    # verdiğimiz inputun içeriğinin sayı olup olmadığını kontrol ediyor.
    elif is_num and inp.isdigit():
        # Eğer input sayı ise verdiğimiz inputu int olarak geri veriyor.
        return int(inp)
    elif is_num and not inp.isdigit():
        # Eğer input bir sayı değilse hata mesajı verip fonksiyonu tekrar çalıştırıyor.
        # Bu sayede sayı dönene kadar kullanıcıdan input bekliyor
        print('Lütfen bir sayı giriniz.')
        return get_input(text, is_num=True)
    # Sıradaki iki kontrolümüz is_opt değeri True olarak tanımlanmışsa geçerlidir.
    elif is_opt and inp in opt_list:
        # Burada verdiğimiz input opt_list listesinin içindeyse eğer inputu direkt olarak çıktı olarak verir.
        return inp
    elif is_opt and inp not in opt_list:
        # Burada ise verdiğimiz input liste içerisinde yoksa önce bir hata mesajı
        # ardından da fonsiyonu tekrar çağırarak kullanıcıdan tekrar bir operatör girmesini bekler.
        print('Lütfen geçerli bir operatör seçiniz:\n', opt_list)
        return get_input(text, is_opt=True, opt_list=opt_list)
    else:
        return inp


def calculate(sayi1, sayi2, op):
    """
    Bu fonksiyonda eval komutunu kullanarak islemi string halinde aldigimiz operatoru kullanarak yapabiliyoruz
    Ancak bu senaryo duzgun bir filtreleme yapmazsak eger kodumuzu buyuk bir riske sokabilir.
    """
    sonuc = eval(f'{sayi1} {op} {sayi2}')
    print(f'{sayi1} {op} {sayi2} = {sonuc}')


def calc_print(sayi1, sayi2, op, sonuc):
    """
    Bu fonskiyon sadece matematiksel işlemimizi ekrana güzel bir formatta
    yazdırmak için mevcuttur.
    """
    print(f'{sayi1} {op} {sayi1} = {sonuc}')


while True:
    sayi1 = get_input('İlk sayı', is_num=True)
    sayi2 = get_input('İkinci sayı', is_num=True)
    operator = get_input('Operatör', is_opt=True)

    calculate(sayi1, sayi2, operator)

    # if operator == '+':
    #     sonuc = sayi1 + sayi2
    #     calc_print(sayi1, sayi2, operator, sonuc)
    # elif operator == '-':
    #     sonuc = sayi1 - sayi2
    #     calc_print(sayi1, sayi2, operator, sonuc)
    # elif operator == '*':
    #     sonuc = sayi1 * sayi2
    #     calc_print(sayi1, sayi2, operator, sonuc)
    # elif operator == '/':
    #     sonuc = sayi1 / sayi2
    #     calc_print(sayi1, sayi2, operator, sonuc)
    # elif operator == '%':
    #     sonuc = sayi1 % sayi2
    #     calc_print(sayi1, sayi2, operator, sonuc)
