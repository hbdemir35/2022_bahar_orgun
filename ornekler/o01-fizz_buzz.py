for i in range(1, 51):  # Bu satirda 1den 50ye kadar bir dongu olusturup her adimda sayiyi "i" olarak kaydediyoruz
    if i % 3 == 0 and i % 5 == 0:  # Burada da sayinin 3 VE 5'e bolunup bolunmedigini kontrol ediyoruz.
        print('FizzBuzz')
    elif i % 3 == 0:  # Burada sayinin 3'e bolunup bolunmedigini kontrol ediyoruz.
        print('Fizz')
    elif i % 5 == 0:  # Burada sayinin 5'e bolunup bolunmedigini kontrol ediyoruz.
        print('Buzz')
    else:  # Burada da hicbir kosul dogru degilse calisacak komutumuzu belirliyoruz.
        print(i)


"""
Eger islemciye daha az yuk bildirsin istersek ilk kontroldeki and
yerine burada matematiksel bir kisayol kullanarak 3 ile 5in en kucuk
ortak carpanini kullanabiliriz. Bu durumda kodumuz su sekilde olacaktir:
if i % 15 == 0:
"""
