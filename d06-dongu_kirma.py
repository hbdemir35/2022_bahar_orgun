ogrenciler = ['Deniz', 'Kemal', 'Cemre', 'Suheyb']

for i in ogrenciler:
    if i == 'Cemre':
        continue
    print(i)

print('\n')
for i in ogrenciler:
    if i == 'Cemre':
        break
    print(i)
