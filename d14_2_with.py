# With, kendi içerisinde kapatmaya ihtiyaç duyan metodları
# bir kod bloğu içerisine alarak işi bittiğinde otomatik kapatmayı sağlar

"""
f = open('d14_okunacak_dosya.txt')
print(f.readlines())
f.close()
"""
# Bu iki kod bölüğü de birebir aynı işi yapmaktadır

with open('d14_okunacak_dosya.txt') as f:
    print(f.readlines())

print('Bu noktadan itibaren dosya artik kapali')
